package il.ac.technion.anylocator.listeners;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Class to be run once a place was chosen.
 */
public class OnChoosePlaceListener implements OnItemClickListener {

	private Context context;
	
	public OnChoosePlaceListener(Context context) {
		this.context = context;
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
        Toast.makeText(context, "Chosen: "+str, Toast.LENGTH_SHORT).show();
	}

}
