package il.ac.technion.anylocator.bluetooth;

import android.bluetooth.BluetoothDevice;
/**
 * Represents a single bluetooth device.
 */
public class BluetoothDeviceWrapper
{
	public final BluetoothDevice bluetoothDevice;

	public BluetoothDeviceWrapper(final BluetoothDevice bluetoothDevice)
	{
		this.bluetoothDevice = bluetoothDevice;
	}

	@Override
	public String toString()
	{
		return bluetoothDevice.getName();
	}
}
