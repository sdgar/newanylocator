package il.ac.technion.anylocator.bluetooth;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

public class Utils {
	
	public static byte[] objectToByteArray(Object o) throws IOException
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
		  out = new ObjectOutputStream(bos);   
		  out.writeObject(o);
		  return bos.toByteArray();
		  
		} finally {
		  out.close();
		  bos.close();
		}
	}
	
	public static Object byteArrayToObject(byte[] bs) throws StreamCorruptedException, IOException, ClassNotFoundException
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(bs);
		ObjectInput in = null;
		try {
		  in = new ObjectInputStream(bis);
		  return in.readObject(); 
		} finally {
		  bis.close();
		  in.close();
		}
	}

}
