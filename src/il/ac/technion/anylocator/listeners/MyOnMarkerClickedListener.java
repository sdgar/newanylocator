package il.ac.technion.anylocator.listeners;

import il.ac.technion.anylocator.MainActivity;
import il.ac.technion.anylocator.places.Place;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.Marker;

/**
 * Class to be executed once a marker was clicked.
 */
public class MyOnMarkerClickedListener implements OnMarkerClickListener {

	private Context context;
	
	public MyOnMarkerClickedListener(Context context){
		this.context = context;
	}
	
	@Override
	public boolean onMarkerClick(Marker marker) {
		double index = Double.parseDouble(marker.getSnippet());
		Place p = MainActivity.placesInfo.get(index);
		if (null == p)
			new OnSpotClickedListener(context).onMarkerClick(marker);
		else
			new OnPlaceClickedListener(context, p).onMarkerClick(marker);
			
		return false;
	}

}
