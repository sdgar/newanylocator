package il.ac.technion.anylocator.places;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

	private static final String WEBSITE_FIELD = "website";
	private static final String NAME_FIELD = "name";
	private static final String GEOMETRY_FIELD = "geometry";
	private static final String LNG_FIELD = "lng";
	private static final String LAT_FIELD = "lat";
	private static final String URL_FIELD = "url";
	private static final String REFERENCE_FIELD = "reference";
	private static final String DESCRIPTION_FIELD = "description";
	
	private static final String LOG_TAG = "Anylocator";
    
	private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
	private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
	private static final String TYPE_DETAILS = "/details";
	private static final String OUT_JSON = "/json";

	private static final String API_KEY = "AIzaSyADoDCZDxhMhtQl1m4gqVmz5RTDVJ1IlBQ";
	
	private Map<String, Place> lastSearchResult;

	
	private ArrayList<String> resultList;
	
	public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		lastSearchResult = new HashMap<String, Place>();
	}
	
    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    resultList = autocomplete(constraint.toString());
                    
                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }

    
    private ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;
        
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?sensor=false&key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            
            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            
            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            setLastSearchResult(new HashMap<String, Place>());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).getString(DESCRIPTION_FIELD));
                addToSearchResult(predsJsonArray.getJSONObject(i));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }
        
        return resultList;
    }

    /**
     * Gets an encoded place.
     * The method parses it and adds it to the lastSearchResult member
     * 
     * @param encodedPlace
     * @throws JSONException
     */
    private void addToSearchResult(JSONObject encodedPlace) throws JSONException {
    	Place p = new Place();
    	p.setDescription(encodedPlace.getString(DESCRIPTION_FIELD));
    	p.setReference(encodedPlace.getString(REFERENCE_FIELD));
    	lastSearchResult.put(p.getDescription(), p);
    }
    
	public Map<String, Place> getLastSearchResult() {
		return lastSearchResult;
	}

	public void setLastSearchResult(Map<String, Place> lastSearchResult) {
		this.lastSearchResult = lastSearchResult;
	}
	
	public Place getDetails(Place p) {
        
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

		StrictMode.setThreadPolicy(policy); 
		
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_DETAILS + OUT_JSON);
            sb.append("?sensor=false&key=" + API_KEY);
            sb.append("&reference=" + URLEncoder.encode(p.getReference(), "utf8"));
            
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            
            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return null;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return null;
        } catch (Exception e) {
        	Log.e(LOG_TAG, "Exception OhOh!");
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString()).getJSONObject("result");
            
            // Extract the Place descriptions from the results
        	JSONObject jsonLocation = jsonObj.getJSONObject(GEOMETRY_FIELD).getJSONObject("location");
        	p.setLat(jsonLocation.getDouble(LAT_FIELD));
        	p.setLng(jsonLocation.getDouble(LNG_FIELD));
        	p.setUrl(jsonObj.getString(URL_FIELD));
        	p.setName(jsonObj.getString(NAME_FIELD));
        	try {
        		p.setWebsite(jsonObj.getString(WEBSITE_FIELD));
        	} catch(JSONException e) {}
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }
        
        return p;
	}
    
}
