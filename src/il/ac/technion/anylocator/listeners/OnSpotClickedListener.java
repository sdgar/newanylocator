package il.ac.technion.anylocator.listeners;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import il.ac.technion.anylocator.MainActivity;
import il.ac.technion.anylocator.MarkerInfo;
import il.ac.technion.anylocator.R;
import il.ac.technion.anylocator.bluetooth.BluetoothClientActivity;
import il.ac.technion.anylocator.bluetooth.Utils;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;


/**
 * This class represents the press of a marked spot.
 * opens a new dialog with its functionalities.
 * 
 */
public class OnSpotClickedListener {

	//Elements on the screen
	private Context context;
	private Marker marker;
	private EditText textInput;
	private TextView header;
	private Button getDirectionsButton;
	private Button deleteButton;
	private Button saveButton;
	private Button bluetoothSendButton;
	
	//The dialog itself
	private AlertDialog alertDialog;
	
	//determains if it ths the first touch of the field or not
	private boolean isFirst = true;
	
	public OnSpotClickedListener(Context context) {
		this.context = context;
	}
	
	public boolean onMarkerClick(Marker marker) {
		this.marker = marker;
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.prompt_spot, null);

		initMembers(promptsView);
		
		initButtonsAndText(marker);
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setView(promptsView);
		alertDialog = alertDialogBuilder.create();
		alertDialog.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				isFirst = true;
				
			}
		});
		alertDialog.show();
		return false;
	}

	/**
	 * Initializes the listeners on the buttons and the edit text field
	 * @param marker
	 */
	private void initButtonsAndText(final Marker marker) {
		header.setText(marker.getTitle()+":");
		textInput.setText("Change Spot Name...");
		textInput.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (isFirst) {
					isFirst = false;
					textInput.setText("");
				}
			}
		});
		saveButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClick_save(marker);
				
			}
		});
		getDirectionsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClick_getDirections(marker);
				
			}
		});
		deleteButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClick_delete(marker);
				
			}
		});
		bluetoothSendButton.setOnClickListener(new View.OnClickListener() 
        {
        	public void onClick(View v)
        	{
//        		Intent intent = new Intent(context.getApplicationContext(), BluetoothClientActivity.class);
//        		Bundle b = new Bundle();
//        		b.putDouble(MainActivity.BLUETOOTH_ACTIVITY_BUNDLE_LAT, marker.getPosition().latitude);
//				b.putDouble(MainActivity.BLUETOOTH_ACTIVITY_BUNDLE_LNG, marker.getPosition().longitude);
//				intent.putExtras(b);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        		v.getContext().getApplicationContext().startActivity(intent);
        		MarkerInfo markerInfo = new MarkerInfo(marker);
        		File file = new File(context.getExternalFilesDir(null), "info.almu");
        		OutputStreamWriter out;
				try {
					out = new OutputStreamWriter(new FileOutputStream(file));
        			out.write(new Gson().toJson(markerInfo));
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		file.deleteOnExit();
        		Intent i = new Intent(Intent.ACTION_SEND); 
        		i.setType("text/plain"); 
        		i.setPackage("com.android.bluetooth");
        		i.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        		context.startActivity(i);
        	}
        });
	}

	/**
	 * finds the elements in the xml
	 * @param promptsView
	 */
	private void initMembers(View promptsView) {
		textInput = (EditText) promptsView.findViewById(R.id.editName);
		header = (TextView) promptsView.findViewById(R.id.spotName);
		getDirectionsButton = (Button) promptsView.findViewById(R.id.getDirectionsButton);
		deleteButton = (Button) promptsView.findViewById(R.id.deleteButton);
		saveButton = (Button) promptsView.findViewById(R.id.markPlaceButton);
		bluetoothSendButton = (Button)promptsView.findViewById(R.id.sendButton);
	}
	
	private void onClick_getDirections(Marker m){
		context.startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo: "
				+ m.getPosition().latitude + "," + m.getPosition().longitude)));
	}
	
	private void onClick_delete(Marker m){
		MainActivity.removeMarker(m);
		alertDialog.cancel();
	}
	
	private void onClick_save(Marker m){
		if (!isFirst) {
			m.setTitle(textInput.getText().toString());
			MarkerOptions mo = MainActivity.myMarkers.remove(Double.parseDouble(m.getSnippet()));
			mo.title(m.getTitle());
			MainActivity.myMarkers.put(Double.parseDouble(m.getSnippet()), mo);
		}
		alertDialog.cancel();
	}

}
