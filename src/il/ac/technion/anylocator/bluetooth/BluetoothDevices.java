package il.ac.technion.anylocator.bluetooth;

import java.util.ArrayList;

import android.bluetooth.BluetoothDevice;

/**
 * This class represents a bluetooth devices list, which is actually just a list of BluetoothDeviceWrapper (another class in this package).
 * 
 *
 */
public class BluetoothDevices extends ArrayList<BluetoothDeviceWrapper>
{
	/**
	 * Checks of the object is contained in the list, must be implemented as part of inheritage from ArrayList<?> class.
	 * @param obj
	 * @return
	 */
	@Override
	public boolean contains(Object obj)
	{
		if (super.contains(obj))
			return true;
		if ((obj instanceof BluetoothDeviceWrapper) == false)
			return false;
		
		BluetoothDeviceWrapper btdw = (BluetoothDeviceWrapper)obj;
		for (BluetoothDeviceWrapper device : this)
		{
			if (device.bluetoothDevice.getAddress().equals(btdw.bluetoothDevice.getAddress()))
				return true;
		}
		return false;
	}
	/**
	 * Adding a bluetooth device to the class.
	 * @param btdw The bluetooth to be added to the class.
	 * @return whether it was added or not.
	 */
	@Override
	public boolean add(BluetoothDeviceWrapper btdw)
	{
		if (this.contains(btdw))
			return false;
		return super.add(btdw);
	}
}
