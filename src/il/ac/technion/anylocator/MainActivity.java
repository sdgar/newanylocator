package il.ac.technion.anylocator;

import il.ac.technion.anylocator.bluetooth.BluetoothClientActivity;
import il.ac.technion.anylocator.bluetooth.Utils;
import il.ac.technion.anylocator.listeners.MyOnMapLongClickListener;
import il.ac.technion.anylocator.listeners.MyOnMarkerClickedListener;
import il.ac.technion.anylocator.listeners.OnChoosePlaceListener;
import il.ac.technion.anylocator.listeners.OnClickMarkPlaceListener;
import il.ac.technion.anylocator.places.Place;
import il.ac.technion.anylocator.places.PlacesAutoCompleteAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

 /**
  * This is the main activity class, which is ran once the application is executed. 
  */
public class MainActivity extends FragmentActivity implements OnMyLocationChangeListener {
	
	private static final String[] types = {"Normal Map", "Satellite View", "Terrain View", "Hybrid Map"};
	private static final String SAVED_SPOT = "saved spot";
	public static final double DIFFERENCE_OF_INDEX = 0.001;
	public static GoogleMap map;
	private Location myLocation;
	public static Map<Double, MarkerOptions> myMarkers;
	public static Map<Double, Place> placesInfo = null;
	public static double lastIndex = 0;
	private static int mode = -1;
	private static Set<String> markers;
	private static Set<String> places;
	
	public static final UUID BLUETOOTH_UUID = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
	public static final String BLUETOOTH_NAME = "name";
	public static final String BLUETOOTH_ACTIVITY_BUNDLE_LAT = "lat";
	public static final String BLUETOOTH_ACTIVITY_BUNDLE_LNG = "lng";
	
	private BroadcastReceiver bcr;
	private static final Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
        	lastIndex += DIFFERENCE_OF_INDEX;
        	lastIndex = ((double)((int)(lastIndex*1000)))/1000;
        	LatLng latLng = (LatLng)msg.obj;
	    	MarkerOptions mo = new MarkerOptions().position(latLng).title(SAVED_SPOT).snippet(""+lastIndex);
		    map.addMarker(mo);
		    myMarkers.put(lastIndex, mo);
		    markers.add(new MarkerInfo(mo).toString());
        }
	};
	
	private static ArrayList<BluetoothDevice> mArrayAdapter = new ArrayList<BluetoothDevice>();
	
	/**
	 * This class is responsible for receiving coordinates through bluetooth.
	 * @author Eddie
	 *
	 */
	private class BluetoothServerOnClickListener implements OnClickListener
	{
		private BluetoothAdapter bluetoothAdapter;
		private BluetoothServerSocket serverSocket;
		private final Handler mHandler;
		
		public BluetoothServerOnClickListener(Handler handler){
			this.mHandler = handler;
		}
		
		@Override
		public void onClick(View v)
		{
			bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (bluetoothAdapter == null)
				throw new IllegalAccessError(BluetoothClientActivity.NO_BLUETOOTH);
	      
			/*
			 * If bluetooth is not enabled, the application will ask the user to enable it, and wait for the intent
			 * specifying that bluetooth was enabled. In both cases, the device will open the bluetooth connection as server.
			 */
			if (!bluetoothAdapter.isEnabled() || bluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
			{
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
				bcr = new BroadcastReceiver()
				{
					
					@Override
					public void onReceive(Context context, Intent intent)
					{
						if (BluetoothAdapter.ACTION_SCAN_MODE_CHANGED.equals(intent.getAction()) 
									&& (intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, -1) == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
									&& !(intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_SCAN_MODE, -1) == BluetoothAdapter.SCAN_MODE_CONNECTABLE))
						{
							openAsServer();
						}
					}
				};
				registerReceiver(bcr, new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));
				startActivityForResult(enableBtIntent, 1);
				
				return;
			}
			
			openAsServer();
		}

		/**
		 * This method handles the opening of the device for connection from other bluetooth devices.
		 */
		private void openAsServer()
		{
			try
			{
				serverSocket = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(BLUETOOTH_NAME, BLUETOOTH_UUID);
				
				new Thread()
				{
					public void run()
					{
						try
						{
							BluetoothSocket socket = serverSocket.accept();
							if (socket != null)
							{
								receiveCoordinates(socket);
								socket.close();
							}
						} catch (IOException e)
						{
							//Toast.makeText(getApplicationContext(), "IOException: " + e.getMessage(), Toast.LENGTH_LONG).show();
							int a = 1 + 1;
						}
					}

				}.start();
			} catch (IOException e)
			{
				Toast.makeText(getApplicationContext(), "IOException: " + e.getMessage(), Toast.LENGTH_LONG).show();
			}
		}
		
		/**
		 * This method handles the receival of coordinates from another bluetooth device.
		 * @param socket		- The socket through which the information is received.
		 * @return 
		 * @throws IOException
		 */
		private synchronized void receiveCoordinates(BluetoothSocket socket) throws IOException
		{
			InputStream inputStream = socket.getInputStream();
			while(true)
			{
				int available = inputStream.available();
				if (inputStream.available() == 43)
				{
					byte[] input = new byte[inputStream.available()];
					inputStream.read(input);
					
					try {
						double[] ds = (double[]) Utils.byteArrayToObject(input);
						LatLng latLng = new LatLng(ds[0], ds[1]);
						mHandler.sendMessage(mHandler.obtainMessage(0, latLng));
//				    	MarkerOptions mo = new MarkerOptions().position(latLng).title(SAVED_SPOT).snippet(""+lastIndex);
//					    map.addMarker(mo);
//					    myMarkers.put(lastIndex, mo);
						
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
					
					return;
				}
			}
		}
	}
	
	/**
	 * This method is called when the map is created
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        map.setMyLocationEnabled(true);
        map.setOnMyLocationChangeListener(this);
        
        map.setOnMarkerClickListener(new MyOnMarkerClickedListener(this));
        map.setOnMapLongClickListener(new MyOnMapLongClickListener(this));
        initStaticMaps();
        
        final Button button2 = (Button) findViewById(R.id.shareButton);
        button2.setOnClickListener(new BluetoothServerOnClickListener(mHandler));
        
        if (-1 == mode)
        	mode = map.getMapType();
        else
        	map.setMapType(mode);
    }

    /**
     * Gets all the details saved last time
     */
	private void initStaticMaps() {
		SharedPreferences prefs = getSharedPreferences("Anylocator", MODE_PRIVATE);
		markers = new HashSet<String>();
		markers = prefs.getStringSet("markers", markers);
		places = new HashSet<String>();
		places = prefs.getStringSet("places", places);
		lastIndex = prefs.getFloat("lastIndex", 0);
		lastIndex = ((double)((int)(lastIndex*1000)))/1000;
		myMarkers = new HashMap<Double, MarkerOptions>();
		placesInfo = new HashMap<Double, Place>();
		for (String mo : markers) {
			myMarkers.put(MarkerInfo.getIndex(mo), MarkerInfo.stringToMarkerOptions(mo));
		}
		for (String p : places) {
			Place place = Place.parsePlace(p);
			placesInfo.put(place.getIndex(), place);
		}
        loadMarkers();
        	
	}

	/**
	 * Gets the parameters for the new spot and puts if on the map and saves it.
	 * @param lat
	 * @param lng
	 * @param title
	 */
	private void addGeneralSpot(double lat, double lng, String title) {
    	lastIndex += DIFFERENCE_OF_INDEX;
    	lastIndex = ((double)((int)(lastIndex*1000)))/1000;
    	MarkerOptions mo = new MarkerOptions().position(new LatLng(lat, lng)).title(title).snippet(""+lastIndex);
	    map.addMarker(mo);
	    myMarkers.put(lastIndex, mo);
	}
	
	/**
	 * Gets a location and adds it to the map and saves it.
	 * @param l
	 */
	public void addARandomSpot(LatLng l) {
		addGeneralSpot(l.latitude, l.longitude, SAVED_SPOT);
	}
	
	/**
	 * The onClick method for the button to add the current spot on the map
	 * @param v
	 */
	public void onClick_addThis(View v) {
		addARandomSpot(convertLocation(myLocation));
    }
    
	/**
	 * The onClick method to the button to add a place from Places to the map
	 * @param v
	 */
    public void onClick_addPlace(View v) {
		LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(R.layout.search_place, null);
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		final AlertDialog alertDialog = alertDialogBuilder.create();
		
		PlacesAutoCompleteAdapter adapter = new PlacesAutoCompleteAdapter(promptsView.getContext(), R.layout.item_list);
		AutoCompleteTextView autoCompView = (AutoCompleteTextView) promptsView.findViewById(R.id.atv_places);
	    autoCompView.setAdapter(adapter);
	    autoCompView.setOnItemClickListener(new OnChoosePlaceListener(this));
	    
	    Button cancelButton = (Button)promptsView.findViewById(R.id.deleteButton);
	    Button markButton = (Button)promptsView.findViewById(R.id.markPlaceButton);
	    cancelButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				alertDialog.cancel();
			}
		});
	    
	    markButton.setOnClickListener(new OnClickMarkPlaceListener(promptsView.getContext(), adapter, autoCompView, alertDialog));
	   
		alertDialog.show();
    }
    
    /**
     * The onClick method of the button to change map mode
     * @param v
     */
    public void onClick_mode(View v) {
    	AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
    	dialogBuilder.setTitle("Select a Map Type");
    	dialogBuilder.setItems(types, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				mode = which + 1;
				map.setMapType(mode);
				
			}
		});
    	AlertDialog dialog = dialogBuilder.create();
    	dialog.show();
    }
	
    /**
     * Converting Location to LatLng
     * @param l
     * @return
     */
	private LatLng convertLocation(Location l) {
		return new LatLng(l.getLatitude(), l.getLongitude());
	}

	/**
	 * This method is called when the location is changed
	 */
	@Override
	public void onMyLocationChange(Location location) {
		if (myLocation == null ||
			location.getLatitude() != myLocation.getLatitude() ||
			location.getLongitude() != myLocation.getLongitude()) {
			
			myLocation = location;
			map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())));
		}
	}
	
	/**
	 * Show markers on the map from the saved markers
	 */
	private void loadMarkers() {
		map.clear();
		for (MarkerOptions mo : myMarkers.values())
			map.addMarker(mo);
	}
	
	/**
	 * Adding a place to the map and saving it.
	 * @param p
	 */
	public static void addPlace(Place p) {
		lastIndex += DIFFERENCE_OF_INDEX;
		p.setIndex(lastIndex);
		placesInfo.put(lastIndex, p);
		places.add(p.toString());
		LatLng loc = new LatLng(p.getLat(), p.getLng());
    	MarkerOptions mo = new MarkerOptions().position(loc).title(p.getName()).snippet(""+lastIndex);
	    map.addMarker(mo);
	    myMarkers.put(lastIndex, mo);
	    markers.add(new MarkerInfo(mo).toString());
	}
	
	/**
	 * This method is called when the activity ends
	 */
	@Override
	public void finish() {
		SharedPreferences.Editor editor = getSharedPreferences("Anylocator", MODE_PRIVATE).edit();
		markers = new HashSet<String>();
		places = new HashSet<String>();
		for (MarkerOptions mo : myMarkers.values()) {
			markers.add(new MarkerInfo(mo).toString());
		}
		for (Place p: placesInfo.values()) {
			places.add(p.toString());
		}
		editor.putStringSet("markers", markers);
		editor.putStringSet("places", places);
		editor.putFloat("lastIndex", (float) lastIndex);
		editor.commit();
		super.finish();
	}
	
	/**
	 * removes a marker from the map
	 * @param marker
	 */
	public static void removeMarker(Marker marker) {
		markers.remove(new MarkerInfo(marker).toString());
		myMarkers.remove(Double.parseDouble(marker.getSnippet()));
		marker.remove();
	}
	
	/**
	 * removes a place from the data
	 * @param place
	 */
	public static void removePlace(Place place) {
		places.remove(place.toString());
		placesInfo.remove(place.getIndex());
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	public void onResume(){
		SharedPreferences prefs = getSharedPreferences("Anylocator", MODE_PRIVATE);
		markers = new HashSet<String>();
		markers = prefs.getStringSet("markers", markers);
		places = new HashSet<String>();
		places = prefs.getStringSet("places", places);
		lastIndex = prefs.getFloat("lastIndex", 0);
		lastIndex = ((double)((int)(lastIndex*1000)))/1000;
		myMarkers = new HashMap<Double, MarkerOptions>();
		placesInfo = new HashMap<Double, Place>();
		for (String mo : markers) {
			myMarkers.put(MarkerInfo.getIndex(mo), MarkerInfo.stringToMarkerOptions(mo));
		}
		for (String p : places) {
			Place place = Place.parsePlace(p);
			placesInfo.put(place.getIndex(), place);
		}
        loadMarkers();
        checkOpenningIntent();
		super.onResume();
	}
	
	private void checkOpenningIntent() {
	    final Uri data = getIntent().getData();
	    if (data == null || data.getPath() == null)
	      return;
		String fileContent = readLineAndDeleteFile(data.getPath());
		MarkerInfo markerInfo = new Gson().fromJson(fileContent, MarkerInfo.class);
		if (markerInfo == null)
			return;
		addGeneralSpot(markerInfo.getLat(), markerInfo.getLng(), markerInfo.getTitle());
		//mHandler.sendMessage(mHandler.obtainMessage(0, latLng));
	}
	
	private String readLineAndDeleteFile(String path){
		File file = new File(path);
		try {
			InputStream in = new FileInputStream(file);
			String content = new BufferedReader(new InputStreamReader(in)).readLine();
			in.close();
			file.delete();
			return content;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override 
	protected void onNewIntent(final Intent intent) {
	    super.onNewIntent(intent);
	    setIntent(intent);
	  }


	
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onPause()
	 */
	@Override
	public void onPause() {
		SharedPreferences.Editor editor = getSharedPreferences("Anylocator", MODE_PRIVATE).edit();
		markers = new HashSet<String>();
		places = new HashSet<String>();
		for (MarkerOptions mo : myMarkers.values()) {
			markers.add(new MarkerInfo(mo).toString());
		}
		for (Place p: placesInfo.values()) {
			places.add(p.toString());
		}
		editor.putStringSet("markers", markers);
		editor.putStringSet("places", places);
		editor.putFloat("lastIndex", (float) lastIndex);
		editor.commit();
		super.onPause();
	}
}
