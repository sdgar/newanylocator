package il.ac.technion.anylocator;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class SpotInfo {
	private double lat;
	private double lng;
	private String title;
	private boolean isPlace;
	
	public SpotInfo(Marker m, boolean isPlace) {
		lat = m.getPosition().latitude;
		lng = m.getPosition().longitude;
		title = m.getTitle();
		this.isPlace = isPlace;
	}
	
	public SpotInfo(MarkerOptions m, boolean isPlace) {
		lat = m.getPosition().latitude;
		lng = m.getPosition().longitude;
		title = m.getTitle();
		this.isPlace = isPlace;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isPlace() {
		return isPlace;
	}

	public void setPlace(boolean isPlace) {
		this.isPlace = isPlace;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isPlace ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(lat);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(lng);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpotInfo other = (SpotInfo) obj;
		if (isPlace != other.isPlace)
			return false;
		if (Double.doubleToLongBits(lat) != Double.doubleToLongBits(other.lat))
			return false;
		if (Double.doubleToLongBits(lng) != Double.doubleToLongBits(other.lng))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
}
