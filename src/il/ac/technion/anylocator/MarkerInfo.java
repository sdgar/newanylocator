package il.ac.technion.anylocator;

import java.util.HashMap;
import java.util.Map;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.annotations.Expose;

/**
 * A class for saving the marker information between the runs of the activity
 * @author Daniel
 *
 */
public class MarkerInfo {
	private static Map<String, Integer> parsingConstant;
	private static final String SEPERATE = "|";
	private static final double DEFAULT_LOCATION = 1E6;
	@Expose
	private double lat;
	@Expose
	private double lng;
	@Expose
	private String title;
	private String snippet;
	private double index;
	
	private static void initParsingConstant() {
		parsingConstant = new HashMap<String, Integer>();
		parsingConstant.put("title", 0);
		parsingConstant.put("lat", 1);
		parsingConstant.put("lng", 2);
		parsingConstant.put("snippet", 3);
		parsingConstant.put("index", 4);
	}
	
	public MarkerInfo(){
		lat = DEFAULT_LOCATION;
		lng = DEFAULT_LOCATION;
		title = null;
		snippet = null;
		index = 0;
	}

	public MarkerInfo(Marker marker) {
		lat = marker.getPosition().latitude;
		lng = marker.getPosition().longitude;
		title = marker.getTitle();
		snippet = marker.getSnippet();
		index = Double.parseDouble(snippet);
	}
	
	public MarkerInfo(MarkerOptions options) {
		lat = options.getPosition().latitude;
		lng = options.getPosition().longitude;
		title = options.getTitle();
		snippet = options.getSnippet();
		index = Double.parseDouble(snippet);
	}
	
	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}
	
	public double getIndex() {
		return index;
	}

	public void setIndex(double index) {
		this.index = index;
	}

	@Override
	public String toString() {
		return title+SEPERATE+lat+SEPERATE+lng+SEPERATE+snippet+SEPERATE+index;
	}
	
	public static MarkerOptions stringToMarkerOptions(String options) {
		initParsingConstant();
		String[] fields = options.split("\\"+SEPERATE);
		MarkerOptions mo = new MarkerOptions();
		mo.title(fields[parsingConstant.get("title")]);
		mo.position(new LatLng(Double.parseDouble(fields[parsingConstant.get("lat")]), 
				Double.parseDouble(fields[parsingConstant.get("lng")])));
		mo.snippet(fields[parsingConstant.get("snippet")]);
		return mo;
	}
	
	public static double getIndex(String marker) {
		String[] fields = marker.split("\\"+SEPERATE);
		initParsingConstant();
		return Double.parseDouble(fields[parsingConstant.get("index")]);
	}
}
