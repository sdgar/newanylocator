package il.ac.technion.anylocator.listeners;

import il.ac.technion.anylocator.MainActivity;

import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.model.LatLng;

/**
 * Class for clicking long on the map
 */
public class MyOnMapLongClickListener implements OnMapLongClickListener {

	MainActivity activity;
	
	public MyOnMapLongClickListener(MainActivity mainActivity) {
		activity = mainActivity;
	}

	@Override
	public void onMapLongClick(LatLng point) {
		activity.addARandomSpot(point);
	}

}
