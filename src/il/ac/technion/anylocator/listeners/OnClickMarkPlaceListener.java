package il.ac.technion.anylocator.listeners;

import il.ac.technion.anylocator.MainActivity;
import il.ac.technion.anylocator.places.Place;
import il.ac.technion.anylocator.places.PlacesAutoCompleteAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;
/**
 * Class to be run once there was an actual cluck on the place marker which opens the place menu or adds it.
 */
public class OnClickMarkPlaceListener implements OnClickListener {

	private static final String ILLEGAL_PLACE_MSG = "Illegal Place";
	private Context context;
	private PlacesAutoCompleteAdapter adapter;
	private AutoCompleteTextView textView;
	private AlertDialog dialog;
	
	public OnClickMarkPlaceListener(final Context context,
			final PlacesAutoCompleteAdapter adapter,
			final AutoCompleteTextView textView, AlertDialog dialog) {
		this.context = context;
		this.adapter = adapter;
		this.textView = textView;
		this.dialog = dialog;
	}
	
	@Override
	public void onClick(View v) {
		Place p = isLegalPlace();
		if (p != null) {
			adapter.getDetails(p);
			MainActivity.addPlace(p);
			dialog.cancel();
		} else {
			Toast.makeText(context, ILLEGAL_PLACE_MSG, Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * returns the chosen place if a legal choice was made, or null otherwise.
	 * @return
	 */
	private Place isLegalPlace() {
		String chosen = textView.getText().toString();
		return adapter.getLastSearchResult().get(chosen);
	}

}
