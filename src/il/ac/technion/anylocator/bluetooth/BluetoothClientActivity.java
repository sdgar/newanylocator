package il.ac.technion.anylocator.bluetooth;

import il.ac.technion.anylocator.MainActivity;
import il.ac.technion.anylocator.R;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

/**
 * This is the class responsible for the activity itself of the bluetooth connection and sending.
 *
 */
public class BluetoothClientActivity extends Activity 
{

	public static final String NO_BLUETOOTH = "ERROR: Device doesn't have bluetooth";
	public static final String START_DISCOVERY_ERROR = "ERROR: Start Discovery for Bluetooth Device failed";
	
	private BluetoothDevices mBtDevices = new BluetoothDevices();
	private ArrayAdapter<BluetoothDeviceWrapper> mArrayAdapter;
	private BluetoothAdapter bluetoothAdapter;
	
	private BluetoothSocket socket;
	
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() 
	{
	    public void onReceive(Context context, Intent intent) 
	    {
	        // When discovery finds a device
	        if (BluetoothDevice.ACTION_FOUND.equals(intent.getAction())) 
	        {
	            // Get the BluetoothDevice object from the Intent
	            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	            // Add the name and address to an array adapter to show in a ListView
	            mBtDevices.add(new BluetoothDeviceWrapper(device));
	            mArrayAdapter.notifyDataSetChanged();
	        }
	    }
	};
	/**
	 * This function initiates a bluetooth sending operation.
	 * @param btd - The bluetooth device to send to.
	 * @param lat - Optional , latitude of sender.
	 * @param lng - Optional , longtitue of sender.
	 */
	private void sendViaBluetooth(BluetoothDevice btd, final double lat, final double lng)
	{
		bluetoothAdapter.cancelDiscovery();
		try
		{
			socket = btd.createRfcommSocketToServiceRecord(MainActivity.BLUETOOTH_UUID);
//			Method m = btd.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
//	        final BluetoothSocket socket = (BluetoothSocket) m.invoke(btd, 1);
			new Thread()
			{
				public void run()
				{
					try
					{
						double[] cords = new double[2];
						cords[0] = lat;
						cords[1] = lng;
						final byte[] bs = Utils.objectToByteArray(cords);
						socket.connect();
						socket.getOutputStream().write(bs);
						//socket.close();
					} catch (IOException e)
					{
						//Toast.makeText(getApplicationContext(), "IOException: " + e.getMessage(), Toast.LENGTH_LONG).show();
						int a = 2;
					}
				}
			}.start();
		} catch (IOException e)
		{
			Toast.makeText(getApplicationContext(), "IOException: " + e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	/**
	 * This is the function called on the creating of a bluetooth activity of the client.
	 * @param savedInstanceState - the state of the instance for the bluetooth activity.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bluetooth);
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (bluetoothAdapter == null)
			throw new IllegalAccessError(NO_BLUETOOTH);
      
		/*
		 * If bluetooth is not enabled, enable it.
		 */
		if (!bluetoothAdapter.isEnabled())
		{
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, 1);
		}
		
		/*
		 * Register for the event that another bluetooth device was found.
		 */
		registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
		
		/*
		 * Register an adapter to the ListView to show the found bluetooth devices.
		 */
		mArrayAdapter = 
					new ArrayAdapter<BluetoothDeviceWrapper>(this, android.R.layout.simple_list_item_1, mBtDevices);
		ListView deviceListView = (ListView)findViewById(R.id.listview_bluetooth_devices);
		deviceListView.setAdapter(mArrayAdapter);
		deviceListView.setOnItemClickListener(new OnItemClickListener() 
													{
														public void onItemClick(AdapterView parent, View v, int position, long id) 
														{
															sendViaBluetooth(	mArrayAdapter.getItem(position).bluetoothDevice,
																				getIntent().getExtras().getDouble(MainActivity.BLUETOOTH_ACTIVITY_BUNDLE_LAT),
																				getIntent().getExtras().getDouble(MainActivity.BLUETOOTH_ACTIVITY_BUNDLE_LNG));
														}
													});
		
		/*Button searchButton = (Button)findViewById(R.id.search_button);
		searchButton.setOnClickListener(new OnClickListener()
													{
			
														@Override
														public void onClick(View v) 
														{
															if (bluetoothAdapter == null)
																finish();
															if (bluetoothAdapter.isDiscovering() == true)
																return;
															if (false == bluetoothAdapter.startDiscovery())
																throw new IllegalStateException(START_DISCOVERY_ERROR);
														}
													});
		searchButton.setClickable(true);*/
		Button newButton = (Button)findViewById(R.id.newButton);
		newButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClick_newButton(v);
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bluetooth, menu);
		return true;
	}
	
	void onClick_newButton(View v) {
		if (bluetoothAdapter == null)
			finish();
		if (bluetoothAdapter.isDiscovering() == true)
			return;
		if (false == bluetoothAdapter.startDiscovery())
			throw new IllegalStateException(START_DISCOVERY_ERROR);
	}

}
