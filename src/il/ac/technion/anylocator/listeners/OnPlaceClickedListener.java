package il.ac.technion.anylocator.listeners;

import il.ac.technion.anylocator.MainActivity;
import il.ac.technion.anylocator.R;
import il.ac.technion.anylocator.places.Place;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;
/**
 *  Listener for the  menu of an already existing place, which opens that place's menu when pressing on it from the map.
 */
public class OnPlaceClickedListener {
	//Elements on the screen
	private Context context;
	private TextView header;
	private Button getDirectionsButton;
	private Button deleteButton;
	private Button infoButton;
	private Button websiteButton;
	private Place place;
	
	//The dialog itself
	private AlertDialog alertDialog;
	
	public OnPlaceClickedListener(Context context, Place place) {
		this.context = context;
		this.place = place;
	}
	
	public boolean onMarkerClick(Marker marker) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.prompt_place, null);

		initMembers(promptsView, Double.parseDouble(marker.getSnippet()));
		
		initButtonsAndText(marker);
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setView(promptsView);
		alertDialog = alertDialogBuilder.create();
		alertDialog.show();
		return false;
	}

	/**
	 * Initializes the listeners on the buttons and the edit text field
	 * @param marker
	 */
	private void initButtonsAndText(final Marker marker) {
		header.setText(place.getDescription());
		getDirectionsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClick_getDirections(v);
				
			}
		});
		deleteButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClick_delete(marker);
				
			}
		});
		infoButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClick_info();
				
			}
		});
		websiteButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClick_website();
				
			}
		});
	}

	/**
	 * finds the elements in the xml
	 * @param promptsView
	 */
	private void initMembers(View promptsView, double index) {
		header = (TextView) promptsView.findViewById(R.id.placeSpotName);
		getDirectionsButton = (Button) promptsView.findViewById(R.id.placeGetDirectionsButton);
		deleteButton = (Button) promptsView.findViewById(R.id.placeDeleteButton);
		infoButton = (Button) promptsView.findViewById(R.id.placeInfoButton);
		websiteButton = (Button) promptsView.findViewById(R.id.placeWebsiteButton);
		if (null == place.getWebsite())
			websiteButton.setVisibility(Button.GONE);
		else
			websiteButton.setText("Go To "+place.getName()+" Website");
	}
	
	private void onClick_getDirections(View v){
		context.startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo: "
				+ place.getLat() + "," + place.getLng())));
	}
	
	private void onClick_delete(Marker m){
		MainActivity.removePlace(place);
		MainActivity.removeMarker(m);
		alertDialog.cancel();
	}
	
	private void onClick_info() {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(place.getUrl()));
		context.startActivity(i);
	}
	
	private void onClick_website() {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(place.getWebsite()));
		context.startActivity(i);
	}
}
