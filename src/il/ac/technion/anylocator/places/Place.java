package il.ac.technion.anylocator.places;

import java.util.HashMap;
import java.util.Map;

/**
 * This class represents the information about a place
 * @author Daniel
 *
 */
public class Place {
	private static final String SEPERATE = "|";
	private static Map<String, Integer> parsingConstant;
	public static final double DEFAULT_LOCATION_INFO = 1E6;
	private String description;
	private String reference;
	private double lat;
	private double lng;
	private String url;
	private String name;
	private String website;
	private double index;
	
	public Place() {
		description = null;
		reference = null;
		lat = DEFAULT_LOCATION_INFO;
		lng = DEFAULT_LOCATION_INFO;
		url = null;
		name = null;
		website = null;
		index = 0;
		
		parsingConstant = new HashMap<String, Integer>();
		parsingConstant.put("name", 0);
		parsingConstant.put("description", 1);
		parsingConstant.put("lat", 2);
		parsingConstant.put("lng", 3);
		parsingConstant.put("reference", 4);
		parsingConstant.put("url", 5);
		parsingConstant.put("website", 6);
		parsingConstant.put("index", 7);
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	
	public double getIndex() {
		return index;
	}

	public void setIndex(double index) {
		this.index = index;
	}

	@Override
	public String toString() {
		return name+SEPERATE
				+description+SEPERATE
				+lat+SEPERATE
				+lng+SEPERATE
				+reference+SEPERATE
				+url+SEPERATE
				+website+SEPERATE
				+index;
	}
	
	public static Place parsePlace(String place) {
		Place p = new Place();
		String[] fields = place.split("\\"+SEPERATE);
		p.setName(fields[parsingConstant.get("name")]);
		p.setDescription(fields[parsingConstant.get("description")]);
		p.setLat(Double.parseDouble(fields[parsingConstant.get("lat")]));
		p.setLng(Double.parseDouble(fields[parsingConstant.get("lng")]));
		p.setUrl(fields[parsingConstant.get("url")]);
		p.setWebsite(fields[parsingConstant.get("website")]);
		p.setIndex(Double.parseDouble(fields[parsingConstant.get("index")]));
		return p;
	}
}
